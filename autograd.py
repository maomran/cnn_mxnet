import mxnet as mx
from mxnet import nd, autograd

f = nd.array([[2,4],[6,8]])
x = nd.array([[1,2], [3,4]])
f.attach_grad()
x.attach_grad(grad_req='null')
with autograd.record():
    y = x*2*f
    z = y * x

z.backward()

print(f.grad)


